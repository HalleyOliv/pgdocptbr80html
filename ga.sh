#!/bin/sh
# exit when any command fails
set -e
perl -0777 -i -w -pe "s|<!-- Google Analytics -->.*</body>|<!-- Google tag (gtag.js) -->\n<script async src=\"https://www.googletagmanager.com/gtag/js?id=G-BHM1100TNC\"></script>\n<script>\n window.dataLayer = window.dataLayer \|\| [];\n function gtag(){dataLayer.push(arguments);}\n gtag('js', new Date());\n\ \n gtag('config', 'G-BHM1100TNC');\n</script>\n</body>|s" *.html
