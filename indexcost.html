<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="generator" content="HTML Tidy, see www.w3.org">
<meta http-equiv="Content-Language" content="pt-br">
<meta name="author" content="Traduzido por Halley Pacheco de Oliveira - Câmara Municipal do Rio de Janeiro">
<meta name="description" content="PostgreSQL 8.0.0 - Index Cost Estimation Functions">
<title>Index Cost Estimation Functions</title>
<meta name="GENERATOR" content="Modular DocBook HTML Stylesheet Version 1.79">
<link rev="MADE" href="mailto:halleypo@users.sourceforge.net">
<link rel="HOME" title="Documentação do PostgreSQL 8.0.0" href="index.html">
<link rel="UP" title="Internamente" href="internals.html">
<link rel="PREVIOUS" title="Further Reading" href="geqo-biblio.html">
<link rel="NEXT" title="GiST Indexes" href="gist.html">
<link rel="STYLESHEET" type="text/css" href="stylesheet.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="creation" content="2007-04-09T21:40:30">
</head>
<body class="CHAPTER">
<div id="docHeader">
<div id="docHeaderLogo"><a href="http://pgdocptbr.sourceforge.net" title="Projeto pgdocptbr no SourceForge"><img src="./cab_logo.png" width="206" height="80" alt="Projeto pgdocptbr no SourceForge"></a></div>
</div>

<div>
</div>

<div class="NAVHEADER">
<table summary="Header navigation table" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<th colspan="5" align="center" valign="bottom">Documentação do PostgreSQL 8.0.0</th>
</tr>

<tr>
<td width="10%" align="left" valign="top"><a href="geqo-biblio.html" accesskey="P">Anterior</a> </td>
<td width="10%" align="left" valign="top"><a href="geqo.html">Início</a> </td>
<td width="60%" align="center" valign="bottom"></td>
<td width="10%" align="right" valign="top"><a href="gist.html">Fim</a> </td>
<td width="10%" align="right" valign="top"><a href="gist.html" accesskey="N">Próxima</a> </td>
</tr>
</table>

<hr align="LEFT" width="100%">
</div>

<div class="CHAPTER">
<h1><a name="INDEXCOST"></a>Capítulo 48. Index Cost Estimation Functions</h1>

<div class="NOTE">
<blockquote class="NOTE">
<p><b>Autor:</b> Written by Tom Lane (<span class="EMAIL">&lt;<a href="mailto:tgl@sss.pgh.pa.us">tgl@sss.pgh.pa.us</a>&gt;</span> ) on 2000-01-24</p>
</blockquote>
</div>

<div class="NOTE">
<blockquote class="NOTE">
<p><b>Nota:</b> This must eventually become part of a much larger chapter about writing new index access methods.</p>
</blockquote>
</div>

<p>Every index access method must provide a cost estimation function for use by the planner/optimizer. The procedure OID of this function is given in the <span class="LITERAL">amcostestimate</span> field of the access method's <span class="LITERAL">pg_am</span> entry.</p>

<div class="NOTE">
<blockquote class="NOTE">
<p><b>Nota:</b> Prior to <span class="PRODUCTNAME">PostgreSQL</span> 7.0, a different scheme was used for registering index-specific cost estimation functions.</p>
</blockquote>
</div>

<p>The amcostestimate function is given a list of WHERE clauses that have been determined to be usable with the index. It must return estimates of the cost of accessing the index and the selectivity of the WHERE clauses (that is, the fraction of main-table rows that will be retrieved during the index scan). For simple cases, nearly all the work of the cost estimator can be done by calling standard routines in the optimizer; the point of having an amcostestimate function is to allow index access methods to provide index-type-specific knowledge, in case it is possible to improve on the standard estimates.</p>

<p>Each amcostestimate function must have the signature:</p>

<pre class="PROGRAMLISTING">
void
amcostestimate (Query *root,
                RelOptInfo *rel,
                IndexOptInfo *index,
                List *indexQuals,
                Cost *indexStartupCost,
                Cost *indexTotalCost,
                Selectivity *indexSelectivity,
                double *indexCorrelation);
</pre>

<p>The first four parameters are inputs:</p>

<div class="VARIABLELIST">
<dl>
<dt>root</dt>

<dd>
<p>The query being processed.</p>
</dd>

<dt>rel</dt>

<dd>
<p>The relation the index is on.</p>
</dd>

<dt>index</dt>

<dd>
<p>The index itself.</p>
</dd>

<dt>indexQuals</dt>

<dd>
<p>List of index qual clauses (implicitly ANDed); a NIL list indicates no qualifiers are available.</p>
</dd>
</dl>
</div>

<p>The last four parameters are pass-by-reference outputs:</p>

<div class="VARIABLELIST">
<dl>
<dt>*indexStartupCost</dt>

<dd>
<p>Set to cost of index start-up processing</p>
</dd>

<dt>*indexTotalCost</dt>

<dd>
<p>Set to total cost of index processing</p>
</dd>

<dt>*indexSelectivity</dt>

<dd>
<p>Set to index selectivity</p>
</dd>

<dt>*indexCorrelation</dt>

<dd>
<p>Set to correlation coefficient between index scan order and underlying table's order</p>
</dd>
</dl>
</div>

<p>Note that cost estimate functions must be written in C, not in SQL or any available procedural language, because they must access internal data structures of the planner/optimizer.</p>

<p>The index access costs should be computed in the units used by <tt class="FILENAME">src/backend/optimizer/path/costsize.c</tt>: a sequential disk block fetch has cost 1.0, a nonsequential fetch has cost random_page_cost, and the cost of processing one index row should usually be taken as cpu_index_tuple_cost (which is a user-adjustable optimizer parameter). In addition, an appropriate multiple of cpu_operator_cost should be charged for any comparison operators invoked during index processing (especially evaluation of the indexQuals themselves).</p>

<p>The access costs should include all disk and CPU costs associated with scanning the index itself, but NOT the costs of retrieving or processing the main-table rows that are identified by the index.</p>

<p>The <span class="QUOTE">"start-up cost"</span> is the part of the total scan cost that must be expended before we can begin to fetch the first row. For most indexes this can be taken as zero, but an index type with a high start-up cost might want to set it nonzero.</p>

<p>The indexSelectivity should be set to the estimated fraction of the main table rows that will be retrieved during the index scan. In the case of a lossy index, this will typically be higher than the fraction of rows that actually pass the given qual conditions.</p>

<p>The indexCorrelation should be set to the correlation (ranging between -1.0 and 1.0) between the index order and the table order. This is used to adjust the estimate for the cost of fetching rows from the main table.</p>

<div class="PROCEDURE">
<p><b>Cost Estimation</b></p>

<p>A typical cost estimator will proceed as follows:</p>

<ol type="1">
<li class="STEP">
<p>Estimate and return the fraction of main-table rows that will be visited based on the given qual conditions. In the absence of any index-type-specific knowledge, use the standard optimizer function <span class="FUNCTION">clauselist_selectivity()</span>:</p>

<pre class="PROGRAMLISTING">
*indexSelectivity = clauselist_selectivity(root, indexQuals,
                                           rel-&gt;relid, JOIN_INNER);
</pre>
</li>

<li class="STEP">
<p>Estimate the number of index rows that will be visited during the scan. For many index types this is the same as indexSelectivity times the number of rows in the index, but it might be more. (Note that the index's size in pages and rows is available from the IndexOptInfo struct.)</p>
</li>

<li class="STEP">
<p>Estimate the number of index pages that will be retrieved during the scan. This might be just indexSelectivity times the index's size in pages.</p>
</li>

<li class="STEP">
<p>Compute the index access cost. A generic estimator might do this:</p>

<pre class="PROGRAMLISTING">
    /*
     * Our generic assumption is that the index pages will be read
     * sequentially, so they have cost 1.0 each, not random_page_cost.
     * Also, we charge for evaluation of the indexquals at each index row.
     * All the costs are assumed to be paid incrementally during the scan.
     */
    cost_qual_eval(&amp;index_qual_cost, indexQuals);
    *indexStartupCost = index_qual_cost.startup;
    *indexTotalCost = numIndexPages +
        (cpu_index_tuple_cost + index_qual_cost.per_tuple) * numIndexTuples;
</pre>
</li>

<li class="STEP">
<p>Estimate the index correlation. For a simple ordered index on a single field, this can be retrieved from pg_statistic. If the correlation is not known, the conservative estimate is zero (no correlation).</p>
</li>
</ol>
</div>

<p>Examples of cost estimator functions can be found in <tt class="FILENAME">src/backend/utils/adt/selfuncs.c</tt>.</p>

<p>By convention, the <span class="LITERAL">pg_proc</span> entry for an <span class="LITERAL">amcostestimate</span> function should show eight arguments all declared as <span class="TYPE">internal</span> (since none of them have types that are known to SQL), and the return type is <span class="TYPE">void</span>.</p>
</div>

<div class="NAVFOOTER">
<hr align="LEFT" width="100%">
<table summary="Footer navigation table" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="33%" align="left" valign="top"><a href="geqo-biblio.html" accesskey="P">Anterior</a> </td>
<td width="34%" align="center" valign="top"><a href="index.html" accesskey="H">Principal</a> </td>
<td width="33%" align="right" valign="top"><a href="gist.html" accesskey="N">Próxima</a> </td>
</tr>

<tr>
<td width="33%" align="left" valign="top">Further Reading</td>
<td width="34%" align="center" valign="top"><a href="internals.html" accesskey="U">Acima</a> </td>
<td width="33%" align="right" valign="top">GiST Indexes</td>
</tr>
</table>
</div>

<div><a href="http://sourceforge.net"><img id="imgSourceForge" src="http://sflogo.sourceforge.net/sflogo.php?group_id=130426&amp;type=1" alt="SourceForge.net Logo"></a> <a href="http://jigsaw.w3.org/css-validator/"><img id="imgW3C_CSS" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="CSS válido!"></a></div>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-BHM1100TNC"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());
 
 gtag('config', 'G-BHM1100TNC');
</script>
</body>
</html>

