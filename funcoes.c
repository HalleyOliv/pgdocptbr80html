/*
 * $Id: funcoes.c,v 1.5 2005/11/15 19:25:42 halleypo Exp $
 */
#include "postgres.h" /* declara��es gerais do PostgreSQL */
#include "fmgr.h"     /* macros dos argumentos/resultados */

#define VALOR(char)   ((char) - '0')
#define DIGITO(val)   ((val) + '0')
#define VAREND(__PTR) (VARDATA(__PTR) + VARSIZE(__PTR) - VARHDRSZ - sizeof(char))

/* Prot�tipos para prevenir contra poss�veis advert�ncias do gcc. */

Datum   dv11(PG_FUNCTION_ARGS);
Datum   dv11dig(PG_FUNCTION_ARGS);

Datum   dv10(PG_FUNCTION_ARGS);
Datum   dv10dig(PG_FUNCTION_ARGS);

Datum   cpf(PG_FUNCTION_ARGS);
Datum   cnpj(PG_FUNCTION_ARGS);

Datum   nie(PG_FUNCTION_ARGS);

Datum   mdc(PG_FUNCTION_ARGS);

Datum   concat_text(PG_FUNCTION_ARGS);

/*
 *  Rotina para c�lculo do d�gito verificador m�dulo 11.
 *  Recebe dois argumentos, n�mero e d�gito verificador na forma de caracteres.
 *  Retorna verdade se o d�gito verificador estiver correto, falso caso
 *  contr�rio, ou nulo em caso de erro.
 */

PG_FUNCTION_INFO_V1(dv11);

Datum
dv11(PG_FUNCTION_ARGS) {
    int  digito = 0,    // D�gito verificador
         fator  = 2;    // Fator de multiplica��o
    text *num;          // Primeiro argumento = n�mero
    text *dv;           // Segundo argumento = d�gito verificador
    char *c;            // Ponteiro para os caracteres do n�mero
    /* Verificar o recebimento de argumento nulo */
    if (PG_ARGISNULL(0) || PG_ARGISNULL(1))
        PG_RETURN_NULL();
    /* Receber os argumentos */
    num = PG_GETARG_TEXT_P(0);
    dv  = PG_GETARG_TEXT_P(1);
    /* Verificar o recebimento de argumento vazio */
    if ((VARSIZE(num) == VARHDRSZ) || (VARSIZE(dv) == VARHDRSZ)) {
       PG_RETURN_NULL();
    }
    /* Verificar d�gito verificador n�o d�gito */
    if (!isdigit(*VARDATA(dv))) PG_RETURN_NULL();
    /* Calcular o d�gito verificador */
    for (c = VAREND(num); c >= VARDATA(num); c--) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Verificar se � d�gito
        digito += VALOR(*c) * fator;
        if (++fator > 9) fator = 2;
    }
    digito = 11 - ( digito % 11 );
    if (digito >= 10) digito = 0; // Restos 0 ou 1 d�gito = 0
    // Retornar verdade ou falso
    PG_RETURN_BOOL (digito == VALOR(*VARDATA(dv)));
}

/*
 *  Rotina para c�lculo do d�gito verificador m�dulo 11.
 *  Recebe um argumento, o n�mero na forma de caracteres.
 *  Retorna o d�gito verificador, ou nulo em caso de erro.
 */

PG_FUNCTION_INFO_V1(dv11dig);

Datum
dv11dig(PG_FUNCTION_ARGS) {
    int  digito=0,      // D�gito verificador
         fator=2;       // Fator de multiplica��o
    text *num;          // �nico argumento = n�mero
    char *c;            // Ponteiro para os caracteres do n�mero
    int32 tamanho;      // Tamanho do resultado da fun��o
    text *resultado;    // Ponteiro para o resultado da fun��o
    /* Verificar o recebimento de argumento nulo */
    if (PG_ARGISNULL(0))
        PG_RETURN_NULL();
    /* Receber o argumento */
    num  = PG_GETARG_TEXT_P(0);
    /* Verificar o recebimento de argumento vazio */
    if (VARSIZE(num) == VARHDRSZ) {
        PG_RETURN_NULL();
    }
    /* Calcular o d�gito verificador */
    for (c = VAREND(num); c >= VARDATA(num); c--) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Verificar se � d�gito
        digito += VALOR(*c) * fator;
        if (++fator > 9) fator = 2;
    }
    digito = 11 - ( digito % 11 );
    if (digito >= 10) digito = 0; // Restos 0 ou 1 d�gito = 0
    /* Retornar o d�gito verificador */
    tamanho = VARHDRSZ + sizeof(char);
    resultado = (text *) palloc(tamanho);
    memset((void *) resultado, 0, tamanho);
    VARATT_SIZEP(resultado) = tamanho;
    *VARDATA(resultado) = (char) DIGITO(digito);
    PG_RETURN_TEXT_P(resultado);
}

/*
 *  Rotina para c�lculo do d�gito verificador m�dulo 10 - FEBRABAN
 *  Recebe dois argumentos, n�mero e d�gito verificador na forma de caracteres.
 *  Retorna verdade se o d�gito verificador estiver correto, falso caso
 *  contr�rio, ou nulo em caso de erro.
 */

PG_FUNCTION_INFO_V1(dv10);

Datum
dv10(PG_FUNCTION_ARGS) {
    int  digito = 0,    // D�gito verificador
         fator  = 2,    // Fator de multiplica��o
         produto;       // Produto = d�gito x fator
    text *num;          // Primeiro argumento = n�mero
    text *dv;           // Segundo argumento = d�gito verificador
    char *c;            // Ponteiro para os caracteres do n�mero
    /* Verificar o recebimento de argumento nulo */
    if (PG_ARGISNULL(0) || PG_ARGISNULL(1))
        PG_RETURN_NULL();
    /* Receber os argumentos */
    num = PG_GETARG_TEXT_P(0);
    dv  = PG_GETARG_TEXT_P(1);
    /* Verificar o recebimento de argumento vazio */
    if ((VARSIZE(num) == VARHDRSZ) || (VARSIZE(dv) == VARHDRSZ)) {
       PG_RETURN_NULL();
    }
    /* Verificar d�gito verificador n�o d�gito */
    if (!isdigit(*VARDATA(dv))) PG_RETURN_NULL();
    /* Calcular o d�gito verificador */
    for (c = VAREND(num); c >= VARDATA(num); c--) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Verificar se � d�gito
        produto = VALOR(*c) * fator;
        digito+= produto/10 + produto%10;
        if (--fator < 1) fator = 2;
    }
    digito = 10 - ( digito % 10 );
    if (digito == 10) digito = 0;
    /* Retornar verdade ou falso */
    PG_RETURN_BOOL (digito == VALOR(*VARDATA(dv)));
}

/*
 *  Rotina para c�lculo do d�gito verificador m�dulo 10 - FEBRABAN.
 *  Recebe um argumento, o n�mero na forma de caracteres.
 *  Retorna o d�gito verificador, ou nulo em caso de erro.
 */

PG_FUNCTION_INFO_V1(dv10dig);

Datum
dv10dig(PG_FUNCTION_ARGS) {
    int  digito = 0,    // D�gito verificador
         fator  = 2,    // Fator de multiplica��o
         produto;       // Produto = d�gito x fator
    text *num;          // �nico argumento = n�mero
    char *c;            // Ponteiro para os caracteres do n�mero
    int32 tamanho;      // Tamanho do resultado da fun��o
    text *resultado;    // Ponteiro para o resultado da fun��o
    /* Verificar o recebimento de argumento nulo */
    if (PG_ARGISNULL(0))
        PG_RETURN_NULL();
    /* Receber o argumento */
    num  = PG_GETARG_TEXT_P(0);
    /* Verificar o recebimento de argumento vazio */
    if (VARSIZE(num) == VARHDRSZ) {
        PG_RETURN_NULL();
    }
    /* Calcular o d�gito verificador */
    for (c = VAREND(num); c >= VARDATA(num); c--) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Verificar se � d�gito
        produto = VALOR(*c) * fator;
        digito+= produto/10 + produto%10;
        if (--fator < 1) fator = 2;
    }
    digito = 10 - ( digito % 10 );
    if (digito == 10) digito = 0;
    /* Retornar o d�gito verificador */
    tamanho = VARHDRSZ + sizeof(char);
    resultado = (text *) palloc(tamanho);
    memset((void *) resultado, 0, tamanho);
    VARATT_SIZEP(resultado) = tamanho;
    *VARDATA(resultado) = (char) DIGITO(digito);
    PG_RETURN_TEXT_P(resultado);
}

/*
 *  Rotina para valida��o do CPF.
 *  Recebe um argumento, o n�mero do CPF com oito a onze d�gitos.
 *  Retorna verdade se os d�gitos verificadores do CPF estiverem corretos,
 *  falso caso contr�rio, ou nulo se o argumento for nulo, n�o tiver entre
 *  8 e 11 d�gitos, ou contiver um d�gito n�o num�rico. N�o ser�o considerados
 *  v�lidos os seguintes CPF: 000.000.000-00, 111.111.111-11, 222.222.222-22,
 *  333.333.333-33, 444.444.444-44, 555.555.555-55, 666.666.666-66,
 *  777.777.777-77, 888.888.888-88, 999.999.999-99.
 */

PG_FUNCTION_INFO_V1(cpf);

Datum
cpf(PG_FUNCTION_ARGS) {
    text *num;          // �nico argumento = n�mero do CPF
    bool iguais;        // Todos os d�gitos s�o iguais
    int  fator,         // Fator de multiplica��o
         digito;        // D�gito verificador
    char *cpf;          // N�mero do CPF com 11 d�gitos
    char *c;            // Ponteiro para os caracteres do CPF
    /* Verificar o recebimento de argumento nulo */
    if (PG_ARGISNULL(0))
        PG_RETURN_NULL();
    /* Receber o argumento */
    num  = PG_GETARG_TEXT_P(0);
    /* Verificar se o CPF tem entre 8 e 11 d�gitos */
    if ( ((VARSIZE(num) - VARHDRSZ) > 11*sizeof(char)) ||
         ((VARSIZE(num) - VARHDRSZ) <  8*sizeof(char))
       ) PG_RETURN_NULL();
    /* CPF com 11 d�gitos */
    cpf = (char *) palloc(11*sizeof(char));               // Reservar mem�ria
    strncpy (cpf, "00000000000", 11*sizeof(char));        // Preencher com zeros
    memcpy (cpf+11*sizeof(char)-(VARSIZE(num)-VARHDRSZ),  // Destino
            VARDATA(num),                                 // Origem
            VARSIZE(num)-VARHDRSZ);                       // Comprimento
    /* Verificar se todos os d�gitos s�o iguais */
    iguais = true;
    for (c=cpf; c<cpf+9*sizeof(char); *c++) {
        if (*c != *(c+sizeof(char))) {
           iguais = false;
           break;
        }
    }
    if (iguais) PG_RETURN_BOOL(false);
    /* Validar o primeiro d�gito verificador */
    for (c=cpf, digito=0, fator=10; fator>=2; fator--) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Retornar nulo se n�o for d�gito
        digito += VALOR(*c++) * fator;
    }
    digito = 11 - ( digito % 11 );
    if (digito >= 10) digito = 0; // Restos 0 ou 1 digito = 0
    /* Retornar nulo se o primeiro d�gito verificador n�o for um d�gito */
    if (!isdigit(*c)) PG_RETURN_NULL();
    // Retornar falso se o primeiro d�gito n�o estiver correto
    if (digito != VALOR(*c)) PG_RETURN_BOOL(false);
    /* Validar o segundo d�gito verificador */
    for (c=cpf, digito=0, fator=11; fator>=2; fator--) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Retornar nulo se n�o for d�gito
        digito += VALOR(*c++) * fator;
    }
    digito = 11 - ( digito % 11 );
    if (digito >= 10) digito = 0; // Restos 0 ou 1 digito = 0
    /* Retornar nulo se o segundo d�gito verificador n�o for um d�gito */
    if (!isdigit(*c)) PG_RETURN_NULL();
    // Retornar verdade ou falso de acordo com o segundo d�gito verificador
    PG_RETURN_BOOL (digito == VALOR(*c));
}

/*
 *  Rotina para valida��o do CNPJ.
 *  Recebe um argumento, o n�mero do CNPJ com quatorze d�gitos.
 *  Retorna verdade se os d�gitos verificadores do CNPJ estiverem corretos,
 *  falso caso contr�rio, ou nulo se o argumento for nulo, n�o tiver 14 d�gitos,
 *  ou contiver um d�gito n�o num�rico.
 */

PG_FUNCTION_INFO_V1(cnpj);

Datum
cnpj(PG_FUNCTION_ARGS) {
    text *num;          // �nico argumento = n�mero do CNPJ
    int  fator,         // Fator de multiplica��o
         digito;        // D�gito verificador
    char *c;            // Ponteiro para os caracteres do CNPJ
    /* Verificar o recebimento de argumento nulo */
    if (PG_ARGISNULL(0))
        PG_RETURN_NULL();
    /* Receber o argumento */
    num  = PG_GETARG_TEXT_P(0);
    /* Verificar se o CNPJ tem 14 d�gitos */
    if ( (VARSIZE(num) - VARHDRSZ) != 14*sizeof(char) ) {
       PG_RETURN_NULL();
    }
    /* Validar o primeiro d�gito verificador */
    for (c=VARDATA(num), digito=0, fator=13; fator>=2; fator--) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Retornar nulo se n�o for d�gito
        digito += VALOR(*c++) * (fator>9 ? fator-8 : fator);
    }
    digito = 11 - ( digito % 11 );
    if (digito >= 10) digito = 0; // Restos 0 ou 1 digito = 0
    // Retornar falso se o primeiro d�gito n�o estiver correto
    if (digito != VALOR(*c)) PG_RETURN_BOOL(false);
    /* Validar o segundo d�gito verificador */
    for (c=VARDATA(num), digito=0, fator=14; fator>=2; fator--) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Retornar nulo se n�o for d�gito
        digito += VALOR(*c++) * (fator>9 ? fator-8 : fator);
    }
    digito = 11 - ( digito % 11 );
    if (digito >= 10) digito = 0; // Restos 0 ou 1 digito = 0
    // Retornar verdade ou falso de acordo com o segundo d�gito verificador
    PG_RETURN_BOOL (digito == VALOR(*c));
}

/*
 *  Rotina para valida��o do n�mero de inscri��o eleitoral.
 *  Recebe um argumento, o n�mero de inscri��o eleitoral com doze d�gitos.
 *  Retorna verdade se os d�gitos verificadores do n�mero de inscri��o
 *  eleitoral estiverem corretos, falso caso contr�rio, ou nulo se o argumento
 *  for nulo, n�o tiver 12 d�gitos, ou contiver um d�gito n�o num�rico.
 */

PG_FUNCTION_INFO_V1(nie);

Datum
nie(PG_FUNCTION_ARGS) {
    text *num;          // �nico argumento = n�mero de inscri��o eleitoral
    int  fator,         // Fator de multiplica��o
         digito;        // D�gito verificador
    char *nie;          // N�mero do inscri��o eleitoral com 12 d�gitos
    char *c;            // Ponteiro para os caracteres do n�mero de inscri��o
    /* Verificar o recebimento de argumento nulo */
    if (PG_ARGISNULL(0))
        PG_RETURN_NULL();
    /* Receber o argumento */
    num  = PG_GETARG_TEXT_P(0);
    /* Verificar se o n�mero de inscri��o tem entre 10 e 12 d�gitos */
    if ( ((VARSIZE(num) - VARHDRSZ) > 12*sizeof(char)) ||
         ((VARSIZE(num) - VARHDRSZ) < 10*sizeof(char))
       ) PG_RETURN_NULL();
    /* N�mero de inscri��o eleitoral com 12 d�gitos */
    nie = (char *) palloc(12*sizeof(char));               // Reservar mem�ria
    strncpy (nie, "000000000000", 12*sizeof(char));       // Preencher com zeros
    memcpy (nie+12*sizeof(char)-(VARSIZE(num)-VARHDRSZ),  // Destino
            VARDATA(num),                                 // Origem
            VARSIZE(num)-VARHDRSZ);                       // Comprimento
    /* Validar o primeiro d�gito verificador */
    for (c=nie, digito=0, fator=9; fator>=2; fator--) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Retornar nulo se n�o for d�gito
        digito += VALOR(*c++) * fator;
    }
    digito = 11 - ( digito % 11 );
    if (digito >= 10) digito = 0; // Restos 0 ou 1 digito = 0
    // Retornar falso se o primeiro d�gito n�o estiver correto
    if (digito != VALOR(*(c+2*sizeof(char)))) PG_RETURN_BOOL(false);
    /* Validar o segundo d�gito verificador */
    for (digito=0, fator=4; fator>=2; fator--) {
        if (!isdigit(*c)) PG_RETURN_NULL(); // Retornar nulo se n�o for d�gito
        digito += VALOR(*c++) * fator;
    }
    digito = 11 - ( digito % 11 );
    if (digito >= 10) digito = 0; // Restos 0 ou 1 digito = 0
    // Retornar verdade ou falso de acordo com o segundo d�gito verificador
    PG_RETURN_BOOL (digito == VALOR(*c));
}

/*
 *  Rotina para c�lculo do M�ximo Divisor Comum (MDC).
 *  Utiliza o Algoritmo de Euclides ou Algoritmo Euclidiano.
 *  Recebe com par�metro dois n�meros inteiros e retorna o MDC.
 *  Fonte: http://www2.fundao.pro.br/articles.asp?cod=151
 */

PG_FUNCTION_INFO_V1(mdc);

Datum
mdc(PG_FUNCTION_ARGS) {
    int  a,     // Primeiro n�mero
         b,     // Segundo n�mero
         r,     // Resto da divis�o de A por B
         t;     // Armazenamento tempor�rio (troca)
    /* Verificar o recebimento de argumento nulo */
    if (PG_ARGISNULL(0) || PG_ARGISNULL(1))
        PG_RETURN_NULL();
    /* Receber os argumentos */
    a = PG_GETARG_INT32(0);
    b = PG_GETARG_INT32(1);
    /* Garantir que A seja o maior valor */
    if ( a < b ) {
       t = a;
       a = b;
       b = t;
    }
    /* Calcular o MDC */
    while ( b != 0 )
    {
      r = a % b;
      a = b;
      b = r;
    }
    PG_RETURN_INT32(a);
}

/*
 * Fun��o para concatenar duas cadeias de caracteres.
 * Se encontra no arquivo src/tutorial/funcs_new.c da
 * distribui��o do c�digo fonte do PostgreSQL.
 */

PG_FUNCTION_INFO_V1(concat_text);

Datum
concat_text(PG_FUNCTION_ARGS)
{
    text    *arg1 = PG_GETARG_TEXT_P(0);
    text    *arg2 = PG_GETARG_TEXT_P(1);
    int32   tam_novo_texto = VARSIZE(arg1) + VARSIZE(arg2) - VARHDRSZ;
    text    *novo_texto = (text *) palloc(tam_novo_texto);

    memset((void *) novo_texto, 0, tam_novo_texto);
    VARATT_SIZEP(novo_texto) = tam_novo_texto;
    strncpy(VARDATA(novo_texto), VARDATA(arg1), VARSIZE(arg1) - VARHDRSZ);
    strncat(VARDATA(novo_texto), VARDATA(arg2), VARSIZE(arg2) - VARHDRSZ);
    PG_RETURN_TEXT_P(novo_texto);
}
