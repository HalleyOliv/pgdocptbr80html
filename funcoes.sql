---------------------------------------------------------------------------
--
-- funcoes.sql-
--    Exemplos de fun��es escritas em C carregadas dinamicamente.
--
--    Este script carrega ou recarrega o arquivo de biblioteca compartilhada
--    funcoes, cria as fun��es presentes no arquivo de biblioteca, e executa
--    testes de utiliza��o. Deve ser executado por um superusu�rio.
--
-- $Id: funcoes.sql,v 1.3 2005/11/15 19:25:42 halleypo Exp $
--
---------------------------------------------------------------------------

LOAD 'funcoes';

CREATE FUNCTION dv11(text,text) RETURNS boolean
    AS '$libdir/funcoes', 'dv11'
    LANGUAGE C STRICT;

CREATE FUNCTION dv11(text) RETURNS text
    AS '$libdir/funcoes', 'dv11dig'
    LANGUAGE C STRICT;

CREATE FUNCTION dv10(text,text) RETURNS boolean
    AS '$libdir/funcoes', 'dv10'
    LANGUAGE C STRICT;

CREATE FUNCTION dv10(text) RETURNS text
    AS '$libdir/funcoes', 'dv10dig'
    LANGUAGE C STRICT;

CREATE FUNCTION cpf(text) RETURNS boolean
    AS '$libdir/funcoes', 'cpf'
    LANGUAGE C STRICT;

CREATE FUNCTION cnpj(text) RETURNS boolean
    AS '$libdir/funcoes', 'cnpj'
    LANGUAGE C STRICT;

CREATE FUNCTION nie(text) RETURNS boolean
    AS '$libdir/funcoes', 'nie'
    LANGUAGE C STRICT;

CREATE FUNCTION mdc(int, int) RETURNS int
    AS '$libdir/funcoes', 'mdc'
    LANGUAGE C STRICT;

CREATE FUNCTION concat(text, text) RETURNS text
    AS '$libdir/funcoes', 'concat_text'
    LANGUAGE C STRICT;

SELECT dv11('3419112820459284738210122301001623770000034439','7');

SELECT dv11('3419112820459284738210122301001623770000034439');

SELECT dv10('0063504142','9');

SELECT dv10('0063504142');

SELECT cnpj('42498634000166');

SELECT cnpj('42498733000148');

SELECT mdc(144,1024);

SELECT mdc(8192,224);

SELECT concat('Postgre','SQL');

SELECT concat(concat('ae','io'),'u') AS vogais;

SELECT n.nspname, p.proname, p.pronargs, format_type(t.oid, null) as return_type
  FROM pg_namespace n, pg_proc p,
       pg_language l, pg_type t
  WHERE p.pronamespace = n.oid
    and n.nspname not like 'pg\\_%'       -- sem cat�logos
    and n.nspname != 'information_schema' -- sem information_schema
    and p.prolang = l.oid
    and p.prorettype = t.oid
    and l.lanname = 'c'
  ORDER BY nspname, proname, pronargs, return_type;
