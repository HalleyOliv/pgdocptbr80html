<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="generator" content="HTML Tidy, see www.w3.org">
<meta http-equiv="Content-Language" content="pt-br">
<meta name="author" content="Traduzido por Halley Pacheco de Oliveira - Câmara Municipal do Rio de Janeiro">
<meta name="description" content="PostgreSQL 8.0.0 - Transações">
<title>Transações</title>
<meta name="GENERATOR" content="Modular DocBook HTML Stylesheet Version 1.79">
<link rev="MADE" href="mailto:halleypo@users.sourceforge.net">
<link rel="HOME" title="Documentação do PostgreSQL 8.0.0" href="index.html">
<link rel="UP" title="Funcionalidades avançadas" href="tutorial-advanced.html">
<link rel="PREVIOUS" title="Chaves estrangeiras" href="tutorial-fk.html">
<link rel="NEXT" title="Herança" href="tutorial-inheritance.html">
<link rel="STYLESHEET" type="text/css" href="stylesheet.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="creation" content="2007-04-09T21:40:30">
</head>
<body class="SECT1">
<div id="docHeader">
<div id="docHeaderLogo"><a href="http://pgdocptbr.sourceforge.net" title="Projeto pgdocptbr no SourceForge"><img src="./cab_logo.png" width="206" height="80" alt="Projeto pgdocptbr no SourceForge"></a></div>
</div>

<div>
</div>

<div class="NAVHEADER">
<table summary="Header navigation table" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<th colspan="5" align="center" valign="bottom">Documentação do PostgreSQL 8.0.0</th>
</tr>

<tr>
<td width="10%" align="left" valign="top"><a href="tutorial-fk.html" accesskey="P">Anterior</a> </td>
<td width="10%" align="left" valign="top"><a href="tutorial-advanced.html">Início</a> </td>
<td width="60%" align="center" valign="bottom">Capítulo 3. Funcionalidades avançadas</td>
<td width="10%" align="right" valign="top"><a href="tutorial-advanced.html">Fim</a> </td>
<td width="10%" align="right" valign="top"><a href="tutorial-inheritance.html" accesskey="N">Próxima</a> </td>
</tr>
</table>

<hr align="LEFT" width="100%">
</div>

<div class="SECT1">
<h1 class="SECT1"><a name="TUTORIAL-TRANSACTIONS">3.4. Transações</a></h1>


<a name="AEN1184"></a> 

<p><i class="FIRSTTERM">Transação</i> é um conceito fundamental de todo sistema de banco de dados. O ponto essencial da transação é englobar vários passos em uma única operação de tudo ou nada. Os estados intermediários entre os passos não são vistos pelas demais transações simultâneas e, se ocorrer alguma falha que impeça a transação chegar até o fim, então nenhum dos passos intermediários irá afetar o banco de dados de forma alguma.</p>

<p>Por exemplo, considere um banco de dados de uma instituição financeira contendo o saldo da conta corrente de vários clientes, assim como o saldo total dos depósitos de cada agência. Suponha que se deseje transferir $100.00 da conta da Alice para a conta do Bob. Simplificando ao extremo, os comandos <acronym class="ACRONYM">SQL</acronym> para esta operação seriam:</p>

<pre class="PROGRAMLISTING">
UPDATE conta_corrente SET saldo = saldo - 100.00
    WHERE nome = 'Alice';
UPDATE filiais SET saldo = saldo - 100.00
    WHERE nome = (SELECT nome_filial FROM conta_corrente WHERE nome = 'Alice');
UPDATE conta_corrente SET saldo = saldo + 100.00
    WHERE nome = 'Bob';
UPDATE filiais SET saldo = saldo + 100.00
    WHERE nome = (SELECT nome_filial FROM conta_corrente WHERE nome = 'Bob');
</pre>

<p>Os detalhes destes comandos não são importantes aqui; o ponto importante é o fato de existirem várias atualizações distintas envolvidas para realizar esta operação tão simples. A contabilidade do banco quer ter certeza que todas estas atualizações foram realizadas, ou que nenhuma delas foi realizada. Com certeza não é interessante que uma falha no sistema faça com que Bob receba $100.00 que não foi debitado da Alice. Além disso, Alice não continuará sendo uma cliente satisfeita se o dinheiro for debitado de sua conta e não for creditado na conta do Bob. É necessário garantir que, caso aconteça algo errado no meio da operação, nenhum dos passos executados até este ponto irá valer. Agrupar as atualizações em uma <i class="FIRSTTERM">transação</i> dá esta garantia. Uma transação é dita como sendo <i class="FIRSTTERM">atômica</i>: do ponto de vista das outras transações, ou a transação acontece por inteiro, ou nada acontece.</p>

<p>Desejamos, também, ter a garantia de estando a transação completa e aceita pelo sistema de banco de dados que a mesma fique definitivamente gravada, e não seja perdida mesmo no caso de acontecer uma pane logo em seguida. Por exemplo, se estiver sendo registrado um saque em dinheiro pelo Bob não se deseja, de forma alguma, que o débito em sua conta corrente desapareça por causa de uma pane ocorrida logo depois do Bob sair da agência. Um banco de dados transacional garante que todas as atualizações realizadas por uma transação ficam registradas em meio de armazenamento permanente (ou seja, em disco), antes da transação ser considerada completa.</p>

<p>Outra propriedade importante dos bancos de dados transacionais está muito ligada à noção de atualizações atômicas: quando várias transações estão executando ao mesmo tempo, nenhuma delas deve enxergar as alterações incompletas efetuadas pelas outras. Por exemplo, se uma transação está ocupada totalizando o saldo de todas as agências, não pode ser visto o débito efetuado na agência da Alice mas ainda não creditado na agência do Bob, nem o contrário. Portanto, as transações devem ser tudo ou nada não apenas em termos do efeito permanente no banco de dados, mas também em termos de visibilidade durante o processamento. As atualizações feitas por uma transação em andamento não podem ser vistas pelas outras transações enquanto não terminar, quando todas as atualizações se tornam visíveis ao mesmo tempo.</p>

<p>No <span class="PRODUCTNAME">PostgreSQL</span> a transação é definida envolvendo os comandos <acronym class="ACRONYM">SQL</acronym> da transação pelos comandos <a href="sql-begin.html">BEGIN</a> e <a href="sql-commit.html">COMMIT</a>. Sendo assim, a nossa transação bancária ficaria:</p>

<pre class="PROGRAMLISTING">
BEGIN;
UPDATE conta_corrente SET saldo = saldo - 100.00
    WHERE nome = 'Alice';
-- etc etc
COMMIT;
</pre>

<p>Se no meio da transação for decidido que esta não deve ser efetivada (talvez porque tenha sido visto que o saldo da Alice ficou negativo), pode ser usado o comando <a href="sql-rollback.html">ROLLBACK</a> em vez do <a href="sql-commit.html">COMMIT</a> para fazer com que todas as atualizações sejam canceladas.</p>

<p>O <span class="PRODUCTNAME">PostgreSQL</span>, na verdade, trata todo comando <acronym class="ACRONYM">SQL</acronym> como sendo executado dentro de uma transação. Se não for emitido o comando <span class="COMMAND">BEGIN</span>, então cada comando possuirá um <span class="COMMAND">BEGIN</span> implícito e, se der tudo certo, um <span class="COMMAND">COMMIT</span>, envolvendo-o. Um grupo de comandos envolvidos por um <span class="COMMAND">BEGIN</span> e um <span class="COMMAND">COMMIT</span> é algumas vezes chamado de <i class="FIRSTTERM">bloco de transação</i>.</p>

<div class="NOTE">
<blockquote class="NOTE">
<p><b>Nota:</b> Algumas bibliotecas cliente emitem um comando <span class="COMMAND">BEGIN</span> e um comando <span class="COMMAND">COMMIT</span> automaticamente, fazendo com que se obtenha o efeito de um bloco de transação sem perguntar se isto é desejado. Verifique a documentação da interface utilizada.</p>
</blockquote>
</div>

<p>É possível controlar os comandos na transação de uma forma mais granular utilizando os <i class="FIRSTTERM">pontos de salvamento</i> (<i class="FOREIGNPHRASE">savepoints</i>). Os pontos de salvamento permitem descartar partes da transação seletivamente, e efetivar as demais partes. Após definir o ponto de salvamento através da instrução <span class="COMMAND">SAVEPOINT</span>, é possível cancelar a transação até o ponto de salvamento, se for necessário, usando <span class="COMMAND">ROLLBACK TO</span>. Todas as alterações no banco de dados efetuadas pela transação entre o estabelecimento do ponto de salvamento e o cancelamento são descartadas, mas as alterações efetuadas antes do ponto de salvamento são mantidas.</p>

<p>Após cancelar até o ponto de salvamento, este ponto de salvamento continua definido e, portanto, é possível cancelar várias vezes. Ao contrário, havendo certeza que não vai ser mais necessário cancelar até o ponto de salvamento, o ponto de salvamento poderá ser liberado, para que o sistema possa liberar alguns recursos. Deve-se ter em mente que liberar ou cancelar até um ponto de salvamento libera, automaticamente, todos os pontos de salvamento definidos após o mesmo.</p>

<p>Tudo isto acontece dentro do bloco de transação e, portanto, nada disso é visto pelas outras sessões do banco de dados. Quando o bloco de transação é efetivado, as ações efetivadas se tornam visíveis como uma unidade para as outras sessões, enquanto as ações desfeitas nunca se tornam visíveis.</p>

<p>Recordando o banco de dados da instituição financeira, suponha que tivesse sido debitado $100.00 da conta da Alice e creditado na conta do Bob, e descoberto em seguida que era para ser creditado na conta do Wally. Isso poderia ser feito utilizando um ponto de salvamento, conforme mostrado abaixo:</p>

<pre class="PROGRAMLISTING">
BEGIN;
UPDATE conta_corrente SET saldo = saldo - 100.00
    WHERE nome = 'Alice';
SAVEPOINT meu_ponto_de_salvamento;
UPDATE conta_corrente SET saldo = saldo + 100.00
    WHERE nome = 'Bob';
-- uai ... o certo é na conta do Wally
ROLLBACK TO meu_ponto_de_salvamento;
UPDATE conta_corrente SET saldo = saldo + 100.00
    WHERE nome = 'Wally';
COMMIT;
</pre>

<p>Obviamente este exemplo está simplificado ao extremo, mas é possível efetuar um grau elevado de controle sobre a transação através do uso de pontos de salvamento. Além disso, a instrução <span class="COMMAND">ROLLBACK TO</span> é a única forma de obter novamente o controle sobre um bloco de transação colocado no estado interrompido pelo sistema devido a um erro, fora cancelar completamente e começar tudo de novo.</p>
</div>

<div class="NAVFOOTER">
<hr align="LEFT" width="100%">
<table summary="Footer navigation table" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="33%" align="left" valign="top"><a href="tutorial-fk.html" accesskey="P">Anterior</a> </td>
<td width="34%" align="center" valign="top"><a href="index.html" accesskey="H">Principal</a> </td>
<td width="33%" align="right" valign="top"><a href="tutorial-inheritance.html" accesskey="N">Próxima</a> </td>
</tr>

<tr>
<td width="33%" align="left" valign="top">Chaves estrangeiras</td>
<td width="34%" align="center" valign="top"><a href="tutorial-advanced.html" accesskey="U">Acima</a> </td>
<td width="33%" align="right" valign="top">Herança</td>
</tr>
</table>
</div>

<div><a href="http://sourceforge.net"><img id="imgSourceForge" src="http://sflogo.sourceforge.net/sflogo.php?group_id=130426&amp;type=1" alt="SourceForge.net Logo"></a> <a href="http://jigsaw.w3.org/css-validator/"><img id="imgW3C_CSS" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="CSS válido!"></a></div>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-BHM1100TNC"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());
 
 gtag('config', 'G-BHM1100TNC');
</script>
</body>
</html>

