#!/bin/sh
# exit when any command fails
set -e
sudo rsync --archive --verbose --recursive --update --delete --rsh=ssh /home/halley/Downloads/pg80/ /var/www/html/pg80
