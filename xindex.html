<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="generator" content="HTML Tidy, see www.w3.org">
<meta http-equiv="Content-Language" content="pt-br">
<meta name="author" content="Traduzido por Halley Pacheco de Oliveira - Câmara Municipal do Rio de Janeiro">
<meta name="description" content="PostgreSQL 8.0.0 - Interfacing Extensions To Indexes">
<title>Interfacing Extensions To Indexes</title>
<meta name="GENERATOR" content="Modular DocBook HTML Stylesheet Version 1.79">
<link rev="MADE" href="mailto:halleypo@users.sourceforge.net">
<link rel="HOME" title="Documentação do PostgreSQL 8.0.0" href="index.html">
<link rel="UP" title="Estendendo a linguagem SQL" href="extend.html">
<link rel="PREVIOUS" title="Informações de otimização do operador" href="xoper-optimization.html">
<link rel="NEXT" title="Gatilhos" href="triggers.html">
<link rel="STYLESHEET" type="text/css" href="stylesheet.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="creation" content="2007-04-09T21:40:30">
</head>
<body class="SECT1">
<div id="docHeader">
<div id="docHeaderLogo"><a href="http://pgdocptbr.sourceforge.net" title="Projeto pgdocptbr no SourceForge"><img src="./cab_logo.png" width="206" height="80" alt="Projeto pgdocptbr no SourceForge"></a></div>
</div>

<div>
</div>

<div class="NAVHEADER">
<table summary="Header navigation table" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<th colspan="5" align="center" valign="bottom">Documentação do PostgreSQL 8.0.0</th>
</tr>

<tr>
<td width="10%" align="left" valign="top"><a href="xoper-optimization.html" accesskey="P">Anterior</a> </td>
<td width="10%" align="left" valign="top"><a href="extend.html">Início</a> </td>
<td width="60%" align="center" valign="bottom">Capítulo 31. Estendendo a linguagem SQL</td>
<td width="10%" align="right" valign="top"><a href="extend.html">Fim</a> </td>
<td width="10%" align="right" valign="top"><a href="triggers.html" accesskey="N">Próxima</a> </td>
</tr>
</table>

<hr align="LEFT" width="100%">
</div>

<div class="SECT1">
<h1 class="SECT1"><a name="XINDEX">31.14. Interfacing Extensions To Indexes</a></h1>

<a name="AEN37633"></a> 

<p>The procedures described thus far let you define new types, new functions, and new operators. However, we cannot yet define an index on a column of a new data type. To do this, we must define an <i class="FIRSTTERM">operator class</i> for the new data type. Later in this section, we will illustrate this concept in an example: a new operator class for the B-tree index method that stores and sorts complex numbers in ascending absolute value order.</p>

<div class="NOTE">
<blockquote class="NOTE">
<p><b>Nota:</b> Prior to <span class="PRODUCTNAME">PostgreSQL</span> release 7.3, it was necessary to make manual additions to the system catalogs <span class="CLASSNAME">pg_amop</span>, <span class="CLASSNAME">pg_amproc</span>, and <span class="CLASSNAME">pg_opclass</span> in order to create a user-defined operator class. That approach is now deprecated in favor of using <span class="COMMAND">CREATE OPERATOR CLASS</span>, which is a much simpler and less error-prone way of creating the necessary catalog entries.</p>
</blockquote>
</div>

<div class="SECT2">
<h2 class="SECT2"><a name="XINDEX-IM">31.14.1. Index Methods and Operator Classes</a></h2>

<p>The <span class="CLASSNAME">pg_am</span> table contains one row for every index method (internally known as access method). Support for regular access to tables is built into <span class="PRODUCTNAME">PostgreSQL</span>, but all index methods are described in <span class="CLASSNAME">pg_am</span>. It is possible to add a new index method by defining the required interface routines and then creating a row in <span class="CLASSNAME">pg_am</span> &mdash; but that is far beyond the scope of this chapter.</p>

<p>The routines for an index method do not directly know anything about the data types that the index method will operate on. Instead, an <i class="FIRSTTERM">operator class</i><a name="AEN37654"></a> identifies the set of operations that the index method needs to use to work with a particular data type. Operator classes are so called because one thing they specify is the set of <span class="LITERAL">WHERE</span>-clause operators that can be used with an index (i.e., can be converted into an index-scan qualification). An operator class may also specify some <i class="FIRSTTERM">support procedures</i> that are needed by the internal operations of the index method, but do not directly correspond to any <span class="LITERAL">WHERE</span>-clause operator that can be used with the index.</p>

<p>It is possible to define multiple operator classes for the same data type and index method. By doing this, multiple sets of indexing semantics can be defined for a single data type. For example, a B-tree index requires a sort ordering to be defined for each data type it works on. It might be useful for a complex-number data type to have one B-tree operator class that sorts the data by complex absolute value, another that sorts by real part, and so on. Typically, one of the operator classes will be deemed most commonly useful and will be marked as the default operator class for that data type and index method.</p>

<p>The same operator class name can be used for several different index methods (for example, both B-tree and hash index methods have operator classes named <span class="LITERAL">int4_ops</span>), but each such class is an independent entity and must be defined separately.</p>
</div>

<div class="SECT2">
<h2 class="SECT2"><a name="XINDEX-STRATEGIES">31.14.2. Index Method Strategies</a></h2>

<p>The operators associated with an operator class are identified by <span class="QUOTE">"strategy numbers"</span>, which serve to identify the semantics of each operator within the context of its operator class. For example, B-trees impose a strict ordering on keys, lesser to greater, and so operators like <span class="QUOTE">"less than"</span> and <span class="QUOTE">"greater than or equal to"</span> are interesting with respect to a B-tree. Because <span class="PRODUCTNAME">PostgreSQL</span> allows the user to define operators, <span class="PRODUCTNAME">PostgreSQL</span> cannot look at the name of an operator (e.g., <span class="LITERAL">&lt;</span> or <span class="LITERAL">&gt;=</span>) and tell what kind of comparison it is. Instead, the index method defines a set of <span class="QUOTE">"strategies"</span>, which can be thought of as generalized operators. Each operator class specifies which actual operator corresponds to each strategy for a particular data type and interpretation of the index semantics.</p>

<p>The B-tree index method defines five strategies, shown in <a href="xindex.html#XINDEX-BTREE-STRAT-TABLE">Tabela 31-2</a>.</p>

<div class="TABLE"><a name="XINDEX-BTREE-STRAT-TABLE"></a> 

<p><b>Tabela 31-2. B-tree Strategies</b></p>

<table border="1" class="CALSTABLE">
<col>
<col>
<thead>
<tr>
<th>Operation</th>
<th>Strategy Number</th>
</tr>
</thead>

<tbody>
<tr>
<td>less than</td>
<td>1</td>
</tr>

<tr>
<td>less than or equal</td>
<td>2</td>
</tr>

<tr>
<td>equal</td>
<td>3</td>
</tr>

<tr>
<td>greater than or equal</td>
<td>4</td>
</tr>

<tr>
<td>greater than</td>
<td>5</td>
</tr>
</tbody>
</table>
</div>

<p>Hash indexes express only bitwise equality, and so they use only one strategy, shown in <a href="xindex.html#XINDEX-HASH-STRAT-TABLE">Tabela 31-3</a>.</p>

<div class="TABLE"><a name="XINDEX-HASH-STRAT-TABLE"></a> 

<p><b>Tabela 31-3. Hash Strategies</b></p>

<table border="1" class="CALSTABLE">
<col>
<col>
<thead>
<tr>
<th>Operation</th>
<th>Strategy Number</th>
</tr>
</thead>

<tbody>
<tr>
<td>equal</td>
<td>1</td>
</tr>
</tbody>
</table>
</div>

<p>R-tree indexes express rectangle-containment relationships. They use eight strategies, shown in <a href="xindex.html#XINDEX-RTREE-STRAT-TABLE">Tabela 31-4</a>.</p>

<div class="TABLE"><a name="XINDEX-RTREE-STRAT-TABLE"></a> 

<p><b>Tabela 31-4. R-tree Strategies</b></p>

<table border="1" class="CALSTABLE">
<col>
<col>
<thead>
<tr>
<th>Operation</th>
<th>Strategy Number</th>
</tr>
</thead>

<tbody>
<tr>
<td>left of</td>
<td>1</td>
</tr>

<tr>
<td>left of or overlapping</td>
<td>2</td>
</tr>

<tr>
<td>overlapping</td>
<td>3</td>
</tr>

<tr>
<td>right of or overlapping</td>
<td>4</td>
</tr>

<tr>
<td>right of</td>
<td>5</td>
</tr>

<tr>
<td>same</td>
<td>6</td>
</tr>

<tr>
<td>contains</td>
<td>7</td>
</tr>

<tr>
<td>contained by</td>
<td>8</td>
</tr>
</tbody>
</table>
</div>

<p>GiST indexes are even more flexible: they do not have a fixed set of strategies at all. Instead, the <span class="QUOTE">"consistency"</span> support routine of each particular GiST operator class interprets the strategy numbers however it likes.</p>

<p>Note that all strategy operators return Boolean values. In practice, all operators defined as index method strategies must return type <span class="TYPE">boolean</span>, since they must appear at the top level of a <span class="LITERAL">WHERE</span> clause to be used with an index.</p>

<p>By the way, the <span class="STRUCTFIELD">amorderstrategy</span> column in <span class="CLASSNAME">pg_am</span> tells whether the index method supports ordered scans. Zero means it doesn't; if it does, <span class="STRUCTFIELD">amorderstrategy</span> is the strategy number that corresponds to the ordering operator. For example, B-tree has <span class="STRUCTFIELD">amorderstrategy</span> = 1, which is its <span class="QUOTE">"less than"</span> strategy number.</p>
</div>

<div class="SECT2">
<h2 class="SECT2"><a name="XINDEX-SUPPORT">31.14.3. Index Method Support Routines</a></h2>

<p>Strategies aren't usually enough information for the system to figure out how to use an index. In practice, the index methods require additional support routines in order to work. For example, the B-tree index method must be able to compare two keys and determine whether one is greater than, equal to, or less than the other. Similarly, the R-tree index method must be able to compute intersections, unions, and sizes of rectangles. These operations do not correspond to operators used in qualifications in SQL commands; they are administrative routines used by the index methods, internally.</p>

<p>Just as with strategies, the operator class identifies which specific functions should play each of these roles for a given data type and semantic interpretation. The index method defines the set of functions it needs, and the operator class identifies the correct functions to use by assigning them to the <span class="QUOTE">"support function numbers"</span>.</p>

<p>B-trees require a single support function, shown in <a href="xindex.html#XINDEX-BTREE-SUPPORT-TABLE">Tabela 31-5</a>.</p>

<div class="TABLE"><a name="XINDEX-BTREE-SUPPORT-TABLE"></a> 

<p><b>Tabela 31-5. B-tree Support Functions</b></p>

<table border="1" class="CALSTABLE">
<col>
<col>
<thead>
<tr>
<th>Função</th>
<th>Support Number</th>
</tr>
</thead>

<tbody>
<tr>
<td>Compare two keys and return an integer less than zero, zero, or greater than zero, indicating whether the first key is less than, equal to, or greater than the second.</td>
<td>1</td>
</tr>
</tbody>
</table>
</div>

<p>Hash indexes likewise require one support function, shown in <a href="xindex.html#XINDEX-HASH-SUPPORT-TABLE">Tabela 31-6</a>.</p>

<div class="TABLE"><a name="XINDEX-HASH-SUPPORT-TABLE"></a> 

<p><b>Tabela 31-6. Hash Support Functions</b></p>

<table border="1" class="CALSTABLE">
<col>
<col>
<thead>
<tr>
<th>Função</th>
<th>Support Number</th>
</tr>
</thead>

<tbody>
<tr>
<td>Compute the hash value for a key</td>
<td>1</td>
</tr>
</tbody>
</table>
</div>

<p>R-tree indexes require three support functions, shown in <a href="xindex.html#XINDEX-RTREE-SUPPORT-TABLE">Tabela 31-7</a>.</p>

<div class="TABLE"><a name="XINDEX-RTREE-SUPPORT-TABLE"></a> 

<p><b>Tabela 31-7. R-tree Support Functions</b></p>

<table border="1" class="CALSTABLE">
<col>
<col>
<thead>
<tr>
<th>Função</th>
<th>Support Number</th>
</tr>
</thead>

<tbody>
<tr>
<td>union</td>
<td>1</td>
</tr>

<tr>
<td>intersection</td>
<td>2</td>
</tr>

<tr>
<td>size</td>
<td>3</td>
</tr>
</tbody>
</table>
</div>

<p>GiST indexes require seven support functions, shown in <a href="xindex.html#XINDEX-GIST-SUPPORT-TABLE">Tabela 31-8</a>.</p>

<div class="TABLE"><a name="XINDEX-GIST-SUPPORT-TABLE"></a> 

<p><b>Tabela 31-8. GiST Support Functions</b></p>

<table border="1" class="CALSTABLE">
<col>
<col>
<thead>
<tr>
<th>Função</th>
<th>Support Number</th>
</tr>
</thead>

<tbody>
<tr>
<td>consistent</td>
<td>1</td>
</tr>

<tr>
<td>union</td>
<td>2</td>
</tr>

<tr>
<td>compress</td>
<td>3</td>
</tr>

<tr>
<td>decompress</td>
<td>4</td>
</tr>

<tr>
<td>penalty</td>
<td>5</td>
</tr>

<tr>
<td>picksplit</td>
<td>6</td>
</tr>

<tr>
<td>equal</td>
<td>7</td>
</tr>
</tbody>
</table>
</div>

<p>Unlike strategy operators, support functions return whichever data type the particular index method expects, for example in the case of the comparison function for B-trees, a signed integer.</p>
</div>

<div class="SECT2">
<h2 class="SECT2"><a name="XINDEX-EXAMPLE">31.14.4. An Example</a></h2>

<p>Now that we have seen the ideas, here is the promised example of creating a new operator class. (You can find a working copy of this example in <tt class="FILENAME">src/tutorial/complex.c</tt> and <tt class="FILENAME">src/tutorial/complex.sql</tt> in the source distribution.) The operator class encapsulates operators that sort complex numbers in absolute value order, so we choose the name <span class="LITERAL">complex_abs_ops</span>. First, we need a set of operators. The procedure for defining operators was discussed in <a href="xoper.html">Seção 31.12</a>. For an operator class on B-trees, the operators we require are:</p>

<ul compact="COMPACT">
<li><span>absolute-value less-than (strategy 1)</span></li>

<li><span>absolute-value less-than-or-equal (strategy 2)</span></li>

<li><span>absolute-value equal (strategy 3)</span></li>

<li><span>absolute-value greater-than-or-equal (strategy 4)</span></li>

<li><span>absolute-value greater-than (strategy 5)</span></li>
</ul>

<p>The least error-prone way to define a related set of comparison operators is to write the B-tree comparison support function first, and then write the other functions as one-line wrappers around the support function. This reduces the odds of getting inconsistent results for corner cases. Following this approach, we first write</p>

<pre class="PROGRAMLISTING">
#define Mag(c)  ((c)-&gt;x*(c)-&gt;x + (c)-&gt;y*(c)-&gt;y)

static int
complex_abs_cmp_internal(Complex *a, Complex *b)
{
    double      amag = Mag(a),
                bmag = Mag(b);

    if (amag &lt; bmag)
        return -1;
    if (amag &gt; bmag)
        return 1;
    return 0;
}
</pre>

<p>Now the less-than function looks like</p>

<pre class="PROGRAMLISTING">
PG_FUNCTION_INFO_V1(complex_abs_lt);

Datum
complex_abs_lt(PG_FUNCTION_ARGS)
{
    Complex    *a = (Complex *) PG_GETARG_POINTER(0);
    Complex    *b = (Complex *) PG_GETARG_POINTER(1);

    PG_RETURN_BOOL(complex_abs_cmp_internal(a, b) &lt; 0);
}
</pre>

<p>The other four functions differ only in how they compare the internal function's result to zero.</p>

<p>Next we declare the functions and the operators based on the functions to SQL:</p>

<pre class="PROGRAMLISTING">
CREATE FUNCTION complex_abs_lt(complex, complex) RETURNS bool
    AS '<span class="REPLACEABLE">nome_do_arquivo</span>', 'complex_abs_lt'
    LANGUAGE C IMMUTABLE STRICT;

CREATE OPERATOR &lt; (
   leftarg = complex, rightarg = complex, procedure = complex_abs_lt,
   commutator = &gt; , negator = &gt;= ,
   restrict = scalarltsel, join = scalarltjoinsel
);
</pre>

<p>It is important to specify the correct commutator and negator operators, as well as suitable restriction and join selectivity functions, otherwise the optimizer will be unable to make effective use of the index. Note that the less-than, equal, and greater-than cases should use different selectivity functions.</p>

<p>Other things worth noting are happening here:</p>

<ul>
<li>
<p>There can only be one operator named, say, <span class="LITERAL">=</span> and taking type <span class="TYPE">complex</span> for both operands. In this case we don't have any other operator <span class="LITERAL">=</span> for <span class="TYPE">complex</span>, but if we were building a practical data type we'd probably want <span class="LITERAL">=</span> to be the ordinary equality operation for complex numbers (and not the equality of the absolute values). In that case, we'd need to use some other operator name for <span class="FUNCTION">complex_abs_eq</span>.</p>
</li>

<li>
<p>Although <span class="PRODUCTNAME">PostgreSQL</span> can cope with functions having the same name as long as they have different argument data types, C can only cope with one global function having a given name. So we shouldn't name the C function something simple like <tt class="FILENAME">abs_eq</tt>. Usually it's a good practice to include the data type name in the C function name, so as not to conflict with functions for other data types.</p>
</li>

<li>
<p>We could have made the <span class="PRODUCTNAME">PostgreSQL</span> name of the function <tt class="FILENAME">abs_eq</tt>, relying on <span class="PRODUCTNAME">PostgreSQL</span> to distinguish it by argument data types from any other <span class="PRODUCTNAME">PostgreSQL</span> function of the same name. To keep the example simple, we make the function have the same names at the C level and <span class="PRODUCTNAME">PostgreSQL</span> level.</p>
</li>
</ul>

<p>The next step is the registration of the support routine required by B-trees. The example C code that implements this is in the same file that contains the operator functions. This is how we declare the function:</p>

<pre class="PROGRAMLISTING">
CREATE FUNCTION complex_abs_cmp(complex, complex)
    RETURNS integer
    AS '<span class="REPLACEABLE">nome_do_arquivo</span>'
    LANGUAGE C IMMUTABLE STRICT;
</pre>

<p>Now that we have the required operators and support routine, we can finally create the operator class:</p>

<pre class="PROGRAMLISTING">
CREATE OPERATOR CLASS complex_abs_ops
    DEFAULT FOR TYPE complex USING btree AS
        OPERATOR        1       &lt; ,
        OPERATOR        2       &lt;= ,
        OPERATOR        3       = ,
        OPERATOR        4       &gt;= ,
        OPERATOR        5       &gt; ,
        FUNCTION        1       complex_abs_cmp(complex, complex);
</pre>

<p>And we're done! It should now be possible to create and use B-tree indexes on <span class="TYPE">complex</span> columns.</p>

<p>We could have written the operator entries more verbosely, as in</p>

<pre class="PROGRAMLISTING">
        OPERATOR        1       &lt; (complex, complex) ,
</pre>

<p>but there is no need to do so when the operators take the same data type we are defining the operator class for.</p>

<p>The above example assumes that you want to make this new operator class the default B-tree operator class for the <span class="TYPE">complex</span> data type. If you don't, just leave out the word <span class="LITERAL">DEFAULT</span>.</p>
</div>

<div class="SECT2">
<h2 class="SECT2"><a name="XINDEX-OPCLASS-CROSSTYPE">31.14.5. Cross-Data-Type Operator Classes</a></h2>

<p>So far we have implicitly assumed that an operator class deals with only one data type. While there certainly can be only one data type in a particular index column, it is often useful to index operations that compare an indexed column to a value of a different data type. This is presently supported by the B-tree and GiST index methods.</p>

<p>B-trees require the left-hand operand of each operator to be the indexed data type, but the right-hand operand can be of a different type. There must be a support function having a matching signature. For example, the built-in operator class for type <span class="TYPE">bigint</span> (<span class="TYPE">int8</span>) allows cross-type comparisons to <span class="TYPE">int4</span> and <span class="TYPE">int2</span>. It could be duplicated by this definition:</p>

<pre class="PROGRAMLISTING">
CREATE OPERATOR CLASS int8_ops
DEFAULT FOR TYPE int8 USING btree AS
  -- standard int8 comparisons
  OPERATOR 1 &lt; ,
  OPERATOR 2 &lt;= ,
  OPERATOR 3 = ,
  OPERATOR 4 &gt;= ,
  OPERATOR 5 &gt; ,
  FUNCTION 1 btint8cmp(int8, int8) ,

  -- cross-type comparisons to int2 (smallint)
  OPERATOR 1 &lt; (int8, int2) ,
  OPERATOR 2 &lt;= (int8, int2) ,
  OPERATOR 3 = (int8, int2) ,
  OPERATOR 4 &gt;= (int8, int2) ,
  OPERATOR 5 &gt; (int8, int2) ,
  FUNCTION 1 btint82cmp(int8, int2) ,

  -- cross-type comparisons to int4 (integer)
  OPERATOR 1 &lt; (int8, int4) ,
  OPERATOR 2 &lt;= (int8, int4) ,
  OPERATOR 3 = (int8, int4) ,
  OPERATOR 4 &gt;= (int8, int4) ,
  OPERATOR 5 &gt; (int8, int4) ,
  FUNCTION 1 btint84cmp(int8, int4) ;
</pre>

<p>Notice that this definition <span class="QUOTE">"overloads"</span> the operator strategy and support function numbers. This is allowed (for B-tree operator classes only) so long as each instance of a particular number has a different right-hand data type. The instances that are not cross-type are the default or primary operators of the operator class.</p>

<p>GiST indexes do not allow overloading of strategy or support function numbers, but it is still possible to get the effect of supporting multiple right-hand data types, by assigning a distinct strategy number to each operator that needs to be supported. The <span class="LITERAL">consistent</span> support function must determine what it needs to do based on the strategy number, and must be prepared to accept comparison values of the appropriate data types.</p>
</div>

<div class="SECT2">
<h2 class="SECT2"><a name="XINDEX-OPCLASS-DEPENDENCIES">31.14.6. System Dependencies on Operator Classes</a></h2>

<a name="AEN37909"></a> 

<p><span class="PRODUCTNAME">PostgreSQL</span> uses operator classes to infer the properties of operators in more ways than just whether they can be used with indexes. Therefore, you might want to create operator classes even if you have no intention of indexing any columns of your data type.</p>

<p>In particular, there are SQL features such as <span class="LITERAL">ORDER BY</span> and <span class="LITERAL">DISTINCT</span> that require comparison and sorting of values. To implement these features on a user-defined data type, <span class="PRODUCTNAME">PostgreSQL</span> looks for the default B-tree operator class for the data type. The <span class="QUOTE">"equals"</span> member of this operator class defines the system's notion of equality of values for <span class="LITERAL">GROUP BY</span> and <span class="LITERAL">DISTINCT</span>, and the sort ordering imposed by the operator class defines the default <span class="LITERAL">ORDER BY</span> ordering.</p>

<p>Comparison of arrays of user-defined types also relies on the semantics defined by the default B-tree operator class.</p>

<p>If there is no default B-tree operator class for a data type, the system will look for a default hash operator class. But since that kind of operator class only provides equality, in practice it is only enough to support array equality.</p>

<p>When there is no default operator class for a data type, you will get errors like <span class="QUOTE">"could not identify an ordering operator"</span> if you try to use these SQL features with the data type.</p>

<div class="NOTE">
<blockquote class="NOTE">
<p><b>Nota:</b> In <span class="PRODUCTNAME">PostgreSQL</span> versions before 7.4, sorting and grouping operations would implicitly use operators named <span class="LITERAL">=</span>, <span class="LITERAL">&lt;</span>, and <span class="LITERAL">&gt;</span>. The new behavior of relying on default operator classes avoids having to make any assumption about the behavior of operators with particular names.</p>
</blockquote>
</div>
</div>

<div class="SECT2">
<h2 class="SECT2"><a name="XINDEX-OPCLASS-FEATURES">31.14.7. Special Features of Operator Classes</a></h2>

<p>There are two special features of operator classes that we have not discussed yet, mainly because they are not useful with the most commonly used index methods.</p>

<p>Normally, declaring an operator as a member of an operator class means that the index method can retrieve exactly the set of rows that satisfy a <span class="LITERAL">WHERE</span> condition using the operator. For example,</p>

<pre class="PROGRAMLISTING">
SELECT * FROM table WHERE integer_column &lt; 4;
</pre>

<p>can be satisfied exactly by a B-tree index on the integer column. But there are cases where an index is useful as an inexact guide to the matching rows. For example, if an R-tree index stores only bounding boxes for objects, then it cannot exactly satisfy a <span class="LITERAL">WHERE</span> condition that tests overlap between nonrectangular objects such as polygons. Yet we could use the index to find objects whose bounding box overlaps the bounding box of the target object, and then do the exact overlap test only on the objects found by the index. If this scenario applies, the index is said to be <span class="QUOTE">"lossy"</span> for the operator, and we add <span class="LITERAL">RECHECK</span> to the <span class="LITERAL">OPERATOR</span> clause in the <span class="COMMAND">CREATE OPERATOR CLASS</span> command. <span class="LITERAL">RECHECK</span> is valid if the index is guaranteed to return all the required rows, plus perhaps some additional rows, which can be eliminated by performing the original operator invocation.</p>

<p>Consider again the situation where we are storing in the index only the bounding box of a complex object such as a polygon. In this case there's not much value in storing the whole polygon in the index entry &mdash; we may as well store just a simpler object of type <span class="TYPE">box</span>. This situation is expressed by the <span class="LITERAL">STORAGE</span> option in <span class="COMMAND">CREATE OPERATOR CLASS</span>: we'd write something like</p>

<pre class="PROGRAMLISTING">
CREATE OPERATOR CLASS polygon_ops
    DEFAULT FOR TYPE polygon USING gist AS
        ...
        STORAGE box;
</pre>

<p>At present, only the GiST index method supports a <span class="LITERAL">STORAGE</span> type that's different from the column data type. The GiST <span class="LITERAL">compress</span> and <span class="LITERAL">decompress</span> support routines must deal with data-type conversion when <span class="LITERAL">STORAGE</span> is used.</p>
</div>
</div>

<div class="NAVFOOTER">
<hr align="LEFT" width="100%">
<table summary="Footer navigation table" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="33%" align="left" valign="top"><a href="xoper-optimization.html" accesskey="P">Anterior</a> </td>
<td width="34%" align="center" valign="top"><a href="index.html" accesskey="H">Principal</a> </td>
<td width="33%" align="right" valign="top"><a href="triggers.html" accesskey="N">Próxima</a> </td>
</tr>

<tr>
<td width="33%" align="left" valign="top">Informações de otimização do operador</td>
<td width="34%" align="center" valign="top"><a href="extend.html" accesskey="U">Acima</a> </td>
<td width="33%" align="right" valign="top">Gatilhos</td>
</tr>
</table>
</div>

<div><a href="http://sourceforge.net"><img id="imgSourceForge" src="http://sflogo.sourceforge.net/sflogo.php?group_id=130426&amp;type=1" alt="SourceForge.net Logo"></a> <a href="http://jigsaw.w3.org/css-validator/"><img id="imgW3C_CSS" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="CSS válido!"></a></div>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-BHM1100TNC"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());
 
 gtag('config', 'G-BHM1100TNC');
</script>
</body>
</html>

