<?php
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="generator" content="HTML Tidy, see www.w3.org">
<meta http-equiv="Content-Language" content="pt-br">
<meta name="author" content="Traduzido por Halley Pacheco de Oliveira - C&acirc;mara Municipal do Rio de Janeiro">
<meta name="keywords" content="postgresql manual documenta&ccedil;&atilde;o sql comandos tradu&ccedil;&atilde;o portugu&ecirc;s brasil banco dados sgbd sgbdr sgbdor">
<meta name="description" content="Tradu&ccedil;&atilde;o da Documenta&ccedil;&atilde;o do PostgreSQL 8.0.0 para o Portugu&ecirc;s do Brasil">
<title>Documenta&ccedil;&atilde;o do PostgreSQL 8.0.0</title>
<link rev="MADE" href="mailto:pgsql-docs@postgresql.org">
<link rel="NEXT" title="Pref&aacute;cio" href="preface.html">
<link rel="STYLESHEET" type="text/css" href="stylesheet.css">
<meta name="creation" content="2007-03-04T13:42:37">
</head>
<body class="BOOK">
<div id="docHeader">
<div id="docHeaderLogo"><a href="http://pgdocptbr.sourceforge.net" title="Projeto pgdocptbr no SourceForge"><img src="./cab_logo.png" width="206" height="80" alt="Projeto pgdocptbr no SourceForge"></a></div>
</div>

<div id="docNavSearchContainer">
<div id="docSearch">
<!-- Google CSE Search Box Begins -->
<form id="searchbox_002672633044435958246:bpy8wrjdwg8" action="http://pgdocptbr.sourceforge.net/pg80/google.php">
<input type="hidden" name="cx" value="002672633044435958246:bpy8wrjdwg8">
<input type="hidden" name="cof" value="FORID:11">
<input id="qfront" name="q" type="text">
<input id="submit" type="submit" name="sa" value="Procurar">
</form>
<script type="text/javascript" src="http://www.google.com/coop/cse/brand?form=searchbox_002672633044435958246%3Abpy8wrjdwg8"></script>
<!-- Google CSE Search Box Ends -->
</div>
</div>

<div class="BOOK"><a name="POSTGRES"></a>
<div class="TITLEPAGE">
<h1 class="TITLE"><a name="POSTGRES">Documenta&ccedil;&atilde;o do PostgreSQL 8.0.0</a></h1>
<h3 class="CORPAUTHOR"><a href="http://sourceforge.net/projects/pgdocptbr/" target="_top">Projeto de Tradu&ccedil;&atilde;o para o Portugu&ecirc;s do Brasil</a></h3>
<h3 class="CORPAUTHOR">The PostgreSQL Global Development Group</h3>
<p class="COPYRIGHT"><a href="LEGALNOTICE.html">Copyright</a> &copy; 1996-2005 The PostgreSQL Global Development Group</p>
</div>
</div>

<!-- Google Search Result Snippet Begins -->
<div id="results_002672633044435958246:bpy8wrjdwg8"></div>
<div>
<a style="float:right; margin-bottom:0px;" href="http://sourceforge.net">
<img src="http://sourceforge.net/sflogo.php?group_id=130426&amp;type=2" width="125" height="37" border="0" alt="SourceForge.net Logo">
</a>
<script type="text/javascript">
  var googleSearchIframeName = "results_002672633044435958246:bpy8wrjdwg8";
  var googleSearchFormName = "searchbox_002672633044435958246:bpy8wrjdwg8";
  var googleSearchFrameWidth = 600;
  var googleSearchFrameborder = 0;
  var googleSearchDomain = "www.google.com";
  var googleSearchPath = "/cse";
</script>
<script type="text/javascript" src="http://www.google.com/afsonline/show_afs_search.js"></script>
</div>
<!-- Google Search Result Snippet Ends -->
</body>
</html>
