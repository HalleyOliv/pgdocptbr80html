### Introdução

Páginas Web da documentação do PostgreSQL 8.0 traduzida para o
Português do Brasil, permitindo a todos os interessados consultar,
baixar, colocar em seus próprios sites, distribuir através de CDs,
ou de qualquer outra mídia, esta documentação traduzida.

### Sítio na Web

#### [Documentação do PostgreSQL 8.0](https://pgdocptbr.sourceforge.io/pg80/index.html)
