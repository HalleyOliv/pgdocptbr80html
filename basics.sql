---------------------------------------------------------------------------
--
-- basics.sql-
--    Tutorial b�sico (cria��o de tabelas e manipula��o de dados)
--
--
-- Copyright (c) 1994, Andrew Yu, University of California
--
-- $Id: basics.sql,v 1.2 2005/05/22 12:35:08 halleypo Exp $
--
---------------------------------------------------------------------------

-----------------------------
-- Criar as tabelas:
--	� utilizado o comando CREATE TABLE para criar tabelas base.
--      O PostgreSQL possui o seu pr�prio conjunto de tipos nativos.
--      (Observe que o SQL n�o � sens�vel a mai�sculas/min�sculas)
-----------------------------

CREATE TABLE clima (
        cidade          varchar(80),
        temp_min        int,            -- temperatura m�nima
        temp_max        int,            -- temperatura m�xima
        prcp            real,           -- precipita��o
        data            date
);

CREATE TABLE cidades (
        nome            varchar(80),
        localizacao     point
);

-----------------------------
-- Carregar as tabelas:
--	� utilizado o comando INSERT para inserir uma nova linha na tabela.
--      Existem diversas maneiras de especificar para quais colunas os dados
--      se destinam.
-----------------------------

-- 1. A maneira mais simples � quando a lista de valores corresponde � ordem
--    das colunas especificada no comando CREATE TABLE.

INSERT INTO clima 
    VALUES ('S�o Francisco', 46, 50, 0.25, '1994-11-27');

INSERT INTO cidades 
    VALUES ('S�o Francisco', '(-194.0, 53.0)');

-- 2. Tamb�m pode ser especificada a coluna que os valores correspondem.
--    (As colunas podem ser especificadas em qualquer ordem. Tamb�m podem
--     ser omitidas algumas colunas, por exemplo, precipita��o desconhecida)

INSERT INTO clima (cidade, temp_min, temp_max, prcp, data)
    VALUES ('S�o Francisco', 43, 57, 0.0, '1994-11-29');

INSERT INTO clima (data, cidade, temp_max, temp_min)
    VALUES ('1994-11-29', 'Hayward', 54, 37);

-----------------------------
-- Consultar as tabelas:
--	� utilizado o comando SELECT para trazer os dados. A sintaxe b�sica �
--	SELECT colunas FROM tabelas WHERE predicados.
-----------------------------

-- Um exemplo simples �:

SELECT * FROM clima;

-- Tamb�m podem ser especificadas express�es na lista de destino.
-- (O 'AS coluna' especifica o nome da coluna do resultado, sendo opcional)

SELECT cidade, (temp_max+temp_min)/2 AS temp_media, data FROM clima;

-- Se for desejado trazer linhas que satisfazem a uma determinada condi��o
-- (por exemplo, uma restri��o), a condi��o deve ser especificada na cl�usula
-- WHERE. A consulta abaixo traz o clima de S�o Francisco nos dias de chuva.

SELECT *
    FROM clima
    WHERE cidade = 'S�o Francisco' 
      AND prcp > 0.0;

-- Abaixo est� mostrada uma consulta mais complicada. As linhas duplicadas s�o
-- removidas quando se especifica DISTINCT. A cl�usula ORDER BY especifica �s
-- colunas usadas para classifica��o (S� para ter certeza que o comando abaixo
-- n�o vai confundi-lo, DISTINCT e ORDER BY podem ser usados separadamente)

SELECT DISTINCT cidade
    FROM clima
    ORDER BY cidade;

-----------------------------
-- Jun��es entre tabelas:
--	As consultas podem acessar v�rias tabelas de uma vez, ou acessar 
--      a mesma tabela de tal forma que v�rias inst�ncias da tabela sejam
--      processadas ao mesmo tempo
-----------------------------

-- A consulta abaixo faz a jun��o da tabela clima com a tabela cidades.

SELECT *
    FROM clima, cidades
    WHERE cidade = nome;

-- A consulta abaixo evita a duplicidade do nome da cidade:

SELECT cidade, temp_min, temp_max, prcp, data, localizacao
    FROM clima, cidades
    WHERE cidade = nome;

-- Uma vez que os nomes das colunas s�o todos diferentes, n�o � necess�rio
-- especificar o nome da tabela. Se for desejado ser claro, pode ser feito
-- conforme mostrado abaixo (s�o obtidos os mesmos resultados, obviamente).

SELECT clima.cidade, clima.temp_min, clima.temp_max, clima.prcp, clima.data, cidades.localizacao
    FROM clima, cidades
    WHERE cidades.nome = clima.cidade;

-- Sintaxe de JOIN

SELECT *
    FROM clima JOIN cidades ON (clima.cidade = cidades.nome);

-- Jun��o externa

SELECT *
    FROM clima LEFT OUTER JOIN cidades ON (clima.cidade = cidades.nome);

-- Suponha que se deseje encontrar todos os registros que est�o na faixa
-- de temperatura de outros registros. C1 e C2 s�o aliases para clima.

SELECT C1.cidade, C1.temp_min, C1.temp_max, 
       C2.cidade, C2.temp_min, C2.temp_max
FROM clima C1, clima C2
WHERE C1.temp_min < C2.temp_min 
  AND C1.temp_max > C2.temp_max;

-----------------------------
-- Fun��es de agrega��o
-----------------------------

SELECT max(temp_min)
    FROM clima;

SELECT cidade FROM clima
    WHERE temp_min = (SELECT max(temp_min) FROM clima);

-- Agrega��o com GROUP BY
SELECT cidade, max(temp_min)
    FROM clima 
    GROUP BY cidade;

-- ... e HAVING
SELECT cidade, max(temp_min)
    FROM clima
    GROUP BY cidade
    HAVING max(temp_min) < 40;

-----------------------------
-- Atualizar:
--	� utilizado o comando UPDATE para atualizar os dados.
-----------------------------

-- Suponha ter sido descoberto que as leituras de temperatura estejam todas
-- acrescidas de 2 graus a partir de 28 de novembro. Os dados podem ser
-- atualizados da seguinte maneira:

UPDATE clima
    SET temp_max = temp_max - 2,  temp_min = temp_min - 2
    WHERE data > '1994-11-28';

SELECT * FROM clima;

-----------------------------
-- Excluir:
--	� utilizado o comando DELETE para excuir linhas da tabela.
-----------------------------

-- Suponha que n�o h� mais interesse no clima de Hayward, ent�o pode
-- ser feito o seguinte para excluir estas linhas da tabela.

DELETE FROM clima WHERE cidade = 'Hayward';

SELECT * FROM clima;

-- Tamb�m podem ser excu�das todas as linhas da tabela procedendo conforme
-- mostrado abaixo (� diferente de DROP TABLE que remove a tabela al�m de 
-- remover as linhas).

DELETE FROM clima;

SELECT * FROM clima;

-----------------------------
-- Remover as tabelas:
--	� utilizado o comando DROP TABLE para remover as tabelas.
--      Ap�s isto ser feito, as tabelas n�o podem mais ser utilizadas.
-----------------------------

DROP TABLE clima, cidades;
